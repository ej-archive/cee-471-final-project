% Print out the mesh with all labeled values for easier understanding
fprintf('\nFinite element mesh, with node & element labels ')
fprintf('(nodalmatrix, elemmatrix):\n\n')
fprintf('       x2\n       ^\n       |\n')
for row = meshsize+1:-1:1
   fprintf('%4.2g  ',(row-1)*elemL2)
   for col = 1:meshsize
      fprintf('%2g',nodalmatrix(row,col))
      if (col<meshsize+1 & row>1 & nodalmatrix(row,col) == 0) | ...
            (col<meshsize+1 & row>1 & nodalmatrix(row,col+1) ==0)
         fprintf('      ',nodalmatrix(row,col))
      else
         fprintf('------',nodalmatrix(row,col))
      end
   end
   if row > 1
      fprintf('%2g\n       ',nodalmatrix(row,col+1))
      for col = 1:meshsize
         if col>1 & (nodalmatrix(row,col) == 0 | nodalmatrix(row-1,col) == 0)
            fprintf('        ')
         else
            fprintf('|       ')
         end
      end
      if col < meshsize+1 & nodalmatrix(row,col+1) == 0 | nodalmatrix(row-1,col+1) == 0
         fprintf(' \n       ')
      else
         fprintf('|\n       ')
      end
      for col = 1:meshsize
         if elementmatrix(row-1,col) == 0
            if col>1 & (nodalmatrix(row,col) == 0 | nodalmatrix(row-1,col) == 0)
               fprintf('        ')
            else
               fprintf('|       ')
            end
         else
            fprintf('|  %2g   ',elementmatrix(row-1,col))
         end
      end
      if col<meshsize+1 & (nodalmatrix(row,col+1) == 0 | nodalmatrix(row-1,col+1) == 0)
         fprintf(' \n       ')
      else
         fprintf('|\n       ')
      end
      for col = 1:meshsize
         if col>1 & (nodalmatrix(row,col) == 0 | nodalmatrix(row-1,col) == 0)
            fprintf('        ')
         else
            fprintf('|       ')
         end
      end
      if col<meshsize+1 & (nodalmatrix(row,col+1) == 0 | nodalmatrix(row-1,col+1) == 0)
         fprintf(' \n')
      else
         fprintf('|\n')
      end
   else
      fprintf('%2g --> x1\n',nodalmatrix(row,col+1))
      fprintf(['\n       ', num2str(0:elemL1:L1,'%8.2g'), '\n'])
   end
end
fprintf('\n')
