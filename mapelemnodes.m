% Populate a matrix that maps local element nodes to the global nodes
elemnodes = zeros(Ne, 4); % Initialize an Nx4 matrix
elemnum = 0;
for row = 1:meshsize
   for col = 1:meshsize
      if elementmatrix(row,col) > 0
         elemnum = elemnum + 1;
         elemnodes(elemnum,1) = nodalmatrix(row,col);
         elemnodes(elemnum,2) = nodalmatrix(row,col+1);
         elemnodes(elemnum,3) = nodalmatrix(row+1,col);
         elemnodes(elemnum,4) = nodalmatrix(row+1,col+1);
      end
   end
end
