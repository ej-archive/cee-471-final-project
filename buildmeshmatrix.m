% buildmeshmatrix - Script file to construct the node and element matrices 
% for the membrane shape and define boundary nodes.  

msd = membranesubdivisions;
N = 0; % N = counter for the number of nodes
Nf = 0; % Nf = number of free nodes
Ns = 0; % Ns = number of constrained nodes
Ne = 0; % Ne = number of elements

% Mirror the membrane shape around the x-axis for plotting purposes
temp = zeros(size(membraneshape,1));
for row = 1:size(membraneshape,1)
   temp(row,:) = membraneshape(size(membraneshape,1)-row+1,:);
end
membraneshape = temp;

nodalmatrix = zeros(membranesubdivisions*size(membraneshape,1));
elementmatrix = zeros(membranesubdivisions*size(membraneshape,1)-1);
% Populate the node matrix with simply ones
for row = 1 : size(membraneshape,1)
   for col = 1 : size(membraneshape,2)
      if membraneshape(row,col) == 1
         nodalmatrix(msd*(row-1)+1:msd*(row-1)+msd, ...
            msd*(col-1)+1:msd*(col-1)+msd) = ones(msd);
      end
   end
end

% Number all nodes and find boundary nodes
for row = 1 : size(nodalmatrix,1)
   for col = 1 : size(nodalmatrix,2)
      if nodalmatrix(row,col) == 1
         N = N + 1;
         nodalmatrix(row,col) = N;
         if nodalmatrix(row,col) > 0 & row < size(nodalmatrix,1)
            if nodalmatrix(row+1,col) > 0 & col < size(nodalmatrix,2)
               if nodalmatrix(row,col+1) > 0
                  if nodalmatrix(row+1,col+1) > 0
                     Ne = Ne + 1;
                     elementmatrix(row,col) = Ne;
                  end
               end
            end
         end
         % Traverse around the node in a circle, looking for any 0's.  If
         % found, the node is a boundary node.
         if row==1 | col==1
            boundary = 1;
         else if row == size(nodalmatrix,1) | col == size(nodalmatrix,2)
            boundary = 1;
         else
            j = row-1; k = col-1;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j;     k = k + 1;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j;     k = k + 1;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j + 1; k = k;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j + 1; k = k;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j;     k = k - 1;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j;     k = k - 1;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            j = j - 1; k = k;
            if(j>0 & k>0 & j<=size(nodalmatrix,1) & k<=size(nodalmatrix,2))
            if(nodalmatrix(j,k)) == 0
            boundary = 1;
            else
            boundary = 0;
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
            end
         end
         if boundary == 0;
            Nf = Nf + 1;
            typeofnode(N,1) = 0;
         end
         if boundary == 1;
            Ns = Ns + 1;
            typeofnode(N,1) = 1;
            nodalmatrix(row,col) = -nodalmatrix(row,col);
         end
      end
   end
end

nodalcoords = zeros(N,2);
jf = 0;
js = 0;
for row = 1:size(nodalmatrix,1)
   for col = 1:size(nodalmatrix,2)
      if nodalmatrix(row,col) < 0
         js = js + 1;
         nodalmatrix(row,col) = Nf + js;
         nodalcoords(Nf+js,1) = (col-1)*elemL1;
         nodalcoords(Nf+js,2) = (row-1)*elemL2;
      else if nodalmatrix(row,col) > 0
            jf = jf + 1;
            nodalmatrix(row,col) = jf;
            nodalcoords(jf,1) = (col-1)*elemL1;
            nodalcoords(jf,2) = (row-1)*elemL2;
         end
      end
   end
end
