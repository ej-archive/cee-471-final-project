function [ke fe] = elemstiffness(inode, jnode, elemL1, elemL2, T, p)
% Function ELEMSTIFFNESS
% Eric Jenkins, November 23, 2006
% 
% Synopsis: 
%    [ke fe] = elemstiffness()
%
% Input: (All inputs optional)
%        inode     = the globally determined x1 coordinate for point a
%        jnode     = the globally determined x2 coordinate for point a
%        elemL1    = length of horizontal side of local element
%        elemL2    = length of vertical side of local element
%
% Output: (Rows correspond to sequential iterations: row 1 = first iteration)
%         ke = Element stiffness matrix
%         fe = Element load vector

%B = zeros((meshsize+1)^2, 4); % Declare a B matrix of all zeros
%for j = 1:4
%   B(elemnodes(n, j), j) = 1;
%end
%B

syms z eta x1 x2 ua ub uc ud T p
u = [ua; ub; uc; ud];
phi(1,1) = (1-z)*(1-eta);
phi(2,1) = z*(1-eta);
phi(3,1) = (1-z)*eta;
phi(4,1) = z*eta;

%J = inv(diag([elemL1, elemL2]));
for j = 1:4
   J(:,j) = [diff(phi(j,1),z); diff(phi(j,1), eta)];
end
cursiveB = transpose(J);
gradue = transpose(cursiveB)*u;
gradue = subs(gradue,{z,eta},{(x1-inode)/elemL1,(x2-jnode)/elemL2});
cursiveJ = elemL1*elemL2;
ke = int(int(T*cursiveB*transpose(cursiveB)*cursiveJ,z,0,1),eta,0,1);
fe = int(int(p*phi*cursiveJ,z,0,1),eta,0,1);
