clear all; close all; clc

% User-defined parameters

% membraneshape defines the general shape of the membrane (1's where mesh
% exists, 0's where there will be empty space) - always use a square matrix
% for row = 1:10
%    membraneshape(row,:) = [1 ones(1,row) zeros(1,10-row)];
% end
% membraneshape(row+1,:) = ones(1,11)
membraneshape = ones(4)
                 
membranesubdivisions = 1 % number of elements per side of membraneshape entries

L1 = 1 % Total Length of the membrane in the horizontal direction
L2 = 1 % Total Length of the membrane in the vertical direction

% Start of program code

if size(membraneshape,1) ~= size(membraneshape,2)
   error('membraneshape matrix must be a square matrix')
end
% Set gridsize equal to the number of mesh elements to be generated per side of
% the 

meshsize = membranesubdivisions * size(membraneshape,1) - 1
elemL1 = L1/meshsize
elemL2 = L2/meshsize

buildmeshmatrix;

mapelemnodes;

if meshsize<=20
   printthemesh;
   printelemnodes;
else
   fprintf('\nMesh is too large to print; moving on.\n')
end

%Assembly code per Hjelmstad pg. 226


syms T p val0
K = ones(N, N)*val0; 
f = ones(N, 1)*val0; 
u = ones(N, 1)*val0;

% This following local element code was originally inside the following 'for'
% loop but was taken out since all all elements are rectangular and are the
% same size.  
% (inode,jnode) = point 'a' coordinate of element 1
inode = nodalcoords(elemnodes(1,1),1);
jnode = nodalcoords(elemnodes(1,1),2);
% Retrieve element stiffness matrix for element "n"
[ke,fe] = elemstiffness(inode, jnode, elemL1, elemL2, T, p);

% Loop over all elements to assemble K and f
for n = 1:Ne
   
   % Assemble element stiffness and force vector
   for j=1:4
      for k=1:4
         K(elemnodes(n,j),elemnodes(n,k)) = ...
            K(elemnodes(n,j),elemnodes(n,k)) + ke(j,k);
      end
      if elemnodes(n,j) > Nf
         f(elemnodes(n,j)) = f(elemnodes(n,j)) + fe(j);
      end
   end % loop on i
end % loop on n
K = subs(K,val0,0)
f = subs(f,val0,0)