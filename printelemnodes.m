fprintf('Node coordinates (nodalcoords):\n\n')
fprintf('| node| (  x1,  x2) |\n')
for j = 1:N
   fprintf('|%4g |',j)
   fprintf(' (%4.2g,%4.2g) |',nodalcoords(j,1),nodalcoords(j,2))
   fprintf('\n')
end
fprintf('\n')

fprintf('Element connectivity matrix (elemnodes):\n')
fprintf('|       |      local node       |\n')
fprintf('|element|   a |   b |   c |   d |\n')
for row = 1:Ne
   if row<10
      fprintf(['|     ',num2str([row, elemnodes(row,:)],'%4g |'),'\n'])
   else
      fprintf(['|    ',num2str([row, elemnodes(row,:)],'%4g |'),'\n'])
   end
end
fprintf('\n')
